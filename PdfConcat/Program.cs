using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace PdfConcat
{
    class Program
    {
        static void Main(string[] args)
        {
            var docsPath = Environment.CurrentDirectory + "\\docs\\";
            var resultsPath = Environment.CurrentDirectory + "\\results\\";

            if (!Directory.Exists(docsPath) || !Directory.Exists(resultsPath))
            {
                Directory.CreateDirectory(docsPath);
                Directory.CreateDirectory(resultsPath);
                return;
            }
            
            List<string> fileNames = new List<string>();
            DirectoryInfo d = new DirectoryInfo(docsPath);
            FileInfo[] Files = d.GetFiles("*.pdf");
            string resultName = "";

            foreach (FileInfo file in Files)
            {
                fileNames.Add(docsPath + file.Name);
            }

            if (fileNames.Count < 1)
            {
                Console.WriteLine("Nem taláható file a docs mappában! Nyomjon ENTERT a kilépéshez.");
                if (Console.ReadKey(true).Key == ConsoleKey.Enter)
                {
                    return;
                }
            }

            fileNames = fileNames.OrderBy(f => f).ToList();

            foreach (var file in fileNames)
            {
                Console.WriteLine(file);
            }

            Console.WriteLine("Kezdéshez írja be a cél PDF file nevét, majd nyomjon ENTER-t");
            resultName = Console.ReadLine();

            using (PdfDocument targetDoc = new PdfDocument())
            {
                foreach (string pdf in fileNames)
                {
                    using (PdfDocument pdfDoc = PdfReader.Open(pdf, PdfDocumentOpenMode.Import))
                    {
                        for (int i = 0; i < pdfDoc.PageCount; i++)
                        {
                            targetDoc.AddPage(pdfDoc.Pages[i]);
                        }
                    }
                }
                targetDoc.Save(resultsPath + resultName + ".pdf");
            }

        }
    }
}
